''' module responsible for parsing JAEL code '''

import ply.lex as lex
from utils.config import Config

PROGRAM = None

tokens = ('FUNCTION', 'NUMBER', 'LOOP',
          'REPEAT', 'END', 'FOREACH',
          'IF', 'ELSE', 'NEWLINE')

# Tokens

t_LOOP      = '\['
t_NUMBER    = '-?\d+(/\d+)?'
t_REPEAT    = '\#'
t_FOREACH   = '_'
t_IF        = '[\\?¿]'
t_ELSE      = '\|'
t_END       = '[,]'

# Ignored characters
t_ignore = " \t"

def t_NEWLINE(t):
    r'\n+'

    return t

def t_FUNCTION(t):
    r'[^\[\#_,0-9 \t\d\|\\?¿-]'

    return t
    
def t_error(t):
    print("Illegal character '{:s}'".format(t.value[0]))
    t.lexer.skip(1)
    
# Build the lexer
lexer = lex.lex(optimize=False, debug=False)

# Parsing rules

precedence = (
    )

def p_statement_expr(t):
    'statement : expressions'
    global PROGRAM

    PROGRAM = t[1]

    import json

    if Config.debug['program_structure']:
        print(json.dumps(PROGRAM, sort_keys=False, indent=4))

def p_expressions_expression(t):
    '''expressions : expression expressions'''
    if t[2] is None:
        command = t[1]
    else:
        command = [t[1]]
        if type(t[2]) is list:
            command.extend(t[2])
        else:
            command.append(t[2])

    t[0] = command

def p_expressions_empty(t):
    '''expressions : empty'''
    command = None

    t[0] = command

def p_empty(t):
    'empty :'
    pass

def p_expression_flow(t):
    '''expression : flow END'''
    command = t[1]
    
    if t[2] == ';':
        command = [command, {'type': 'func', 'exec': '.'}]

    t[0] = command

def p_flow_loop(t):
    '''flow : LOOP expressions'''
    command = {'type': 'loop', 'exec': t[2]}
    t[0] = command

def p_flow_repeat(t):
    '''flow : REPEAT expressions'''
    command = {'type': 'repeat', 'exec': t[2]}
    t[0] = command

def p_flow_foreach(t):
    '''flow : FOREACH expressions'''
    command = {'type': 'foreach', 'exec': t[2]}
    t[0] = command

def p_flow_if(t):
    '''flow : IF expressions'''
    execs = [t[2], None]
    if t[1] == '¿':
        execs = execs[::-1]

    command = {'type': 'if', 'exec_true': execs[0], 'exec_false': execs[1]}
    t[0] = command

def p_flow_ifelse(t):
    '''flow : IF expressions ELSE expressions'''
    execs = [t[2], t[4]]
    if t[1] == '¿':
        execs = execs[::-1]

    command = {'type': 'if', 'exec_true': execs[0], 'exec_false': execs[1]}
    t[0] = command

def p_expression_number(t):
    '''expression : NUMBER'''
    command = {'type': 'stack', 'value': eval(t[1]), 'symbol': t[1]}

    t[0] = command

def p_expression_function(t):
    '''expression : FUNCTION'''
    commands = {'type': 'func', 'exec': t[1]}

    t[0] = commands

def p_expression_newline(t):
    '''expression : NEWLINE'''

    command = {'type': 'print'}
    command = None

    t[0] = command

def p_error(t):
    print("Syntax error at '{:s}'".format(t))

    #import sys
    #sys.exit(1)

def parse(code):
    global PROGRAM
    import ply.yacc as yacc

    parser = yacc.yacc(debug=False, write_tables=False)

    parser.parse(code,debug=False)
    return PROGRAM
