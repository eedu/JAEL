''' module responsible for CodeExpander class '''
import abc
import unicodedata as u

from utils.config import Config

from processing.code_completer import CodeCompleter

class CodeExpander:
    ''' class CodeExpander '''

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def extract_code(self):
        ''' :return: the extended version of the code,
            considering the super class capabilities '''
        return ''

    @staticmethod
    def expand(code, complete=False):
        ''' :return: the expanded version of the code,
        using all the known classes '''

        from utils.code_compressor import compare

        if Config.debug['code_expand']:
            print('ORIGINAL CODE\t(size: {:d} {:s}): {:s}'.format(compare(code), Config.compare_mode, code))

        code = ''.join([MappedExtractor(t).extract_code() for t in code])
        code = ''.join([UnicodeAnalyzer(t).extract_code() for t in code])

        if Config.debug['code_expand']:
            print('EXPANDED CODE\t(size: {:d} {:s}): {:s}'.format(compare(code), Config.compare_mode, code))


        if complete:
            if Config.debug['code_complete']:
                Config.debug['code_complete'] = False
                code = CodeCompleter.complete(code)
                Config.debug['code_complete'] = True
                print('COMPLETED CODE\t(size: {:d} {:s}): {:s}'.format(compare(code), Config.compare_mode, code))
            else:
                code = CodeCompleter.complete(code)

        return code

class MappedExtractor(CodeExpander):
    ''' class MappedExtracor '''

    extractions = {
        ';': '.,',
        '⸵': ',.',
        '¨': '..',
        ':': '..',
        '⍩': '¨>',
        '≻': '>!',
        '≺': '<!',
        '⋏': '⋀!',
        '⋎': '⋁!'
    }

    def __init__(self, symbol):
        self.symbol = symbol

    def extract_code(self):
        if self.symbol in MappedExtractor.extractions:
            return MappedExtractor.extractions[self.symbol]

        return self.symbol


class UnicodeAnalyzer(CodeExpander):
    ''' class UnicodeAnalyzer '''

    symbols = {
        'ACUTE': '´',
        'CEDILLA': '¸',
        'CIRCUMFLEX': '^',
        'COMMA': ',',
        'DIAERESIS': '..',
        'DOT': '.',
        'GRAVE': '`',
        'BREVE': '˘',
        'HOOK': '⑀',
        'TILDE': '~',
        'RING': '˚',
        'MACRON': '¯',
        'CARON': 'ˇ',
        'GREATER-THAN': '>',
        'LESS-THAN': '<',
        'CAPITAL': '!',
        'FULL_STOP': '.'
    }

    remove_words = ['APL', 'FUNCTIONAL', 'SYMBOL', 'LATIN', 'SMALL', 'WITH', 'AND']

    replacements = {
        'CEDILLA': 'CEDILLA BELOW',
        'FULL STOP': 'FULL_STOP BELOW',
    }

    pre_modifiers = ['DOUBLE', 'TWO', 'THREE', 'FOUR', 'FIVE']
    post_modifiers = ['BELOW']

    def __init__(self, symbol):

        self.symbol = symbol
        self.name = u.name(symbol, 'NO_NAME_FOUND')
        self.decimal = u.decimal(self.symbol, -1)
        self.digit = u.digit(self.symbol, -1)
        self.numeric = u.numeric(self.symbol, -1)
        self.category = u.category(self.symbol)
        self.bidirectional = u.bidirectional(self.symbol)
        self.combining = u.combining(self.symbol)
        self.east_asian_width = u.east_asian_width(self.symbol)
        self.mirrored = u.mirrored(self.symbol)
        self.decomposition = u.decomposition(self.symbol)
        self.normalize_nfc = u.normalize('NFC', self.symbol)
        self.normalize_nkfc = u.normalize('NFKC', self.symbol)
        self.normalize_nfd = u.normalize('NFD', self.symbol)
        self.normalize_nkfd = u.normalize('NFKD', self.symbol)

        if Config.debug['unicode']:
            self.print_debug()


    @staticmethod
    def __replace_items(name):
        for key, value in UnicodeAnalyzer.replacements.items():
            name = name.replace(key, value)

        return name

    @staticmethod
    def __remove_unnecessary_words(name):
        for word in UnicodeAnalyzer.remove_words:
            name = name.replace(word, '')
            name = name.replace('  ', ' ')
            name = name.replace('  ', ' ')

        name = name.strip()
        return name

    @staticmethod
    def __clear_known_symbol(name, digit):
        import re

        search = re.search('(?:(?<=LETTER )|(?<=CAPITAL LETTER ))[^ ]+', name)

        if search and len(search.group(0)) == 1:
            symbol = search.group(0).lower()
        else:
            search = re.search('(?:(?<=DIGIT ))[^ ]+', name)

            if search:
                symbol = str(digit)
            else:
                symbol = ''

        return symbol

    def extract_code(self):
        import re

        name = self.name

        name = UnicodeAnalyzer.__replace_items(name)
        name = UnicodeAnalyzer.__remove_unnecessary_words(name)
        symbol = UnicodeAnalyzer.__clear_known_symbol(name, self.digit)

        match_pattern = '(({:s}) )?({:s})( ({:s}))?'
        match_symbols = match_pattern.format('|'.join(UnicodeAnalyzer.pre_modifiers),
                                             '|'.join(list(UnicodeAnalyzer.symbols.keys())),
                                             '|'.join(UnicodeAnalyzer.post_modifiers))

        ms_re = re.compile(match_symbols)

        result = ms_re.findall(name)

        for res in result:
            element = {}
            element['multiplier'] = 1

            for word in res:
                if word in UnicodeAnalyzer.pre_modifiers and self.category != 'No':
                    if word == 'TWO':
                        element['multiplier'] = 2
                    elif word == 'THREE':
                        element['multiplier'] = 3
                    elif word == 'FOUR':
                        element['multiplier'] = 4
                    elif word == 'FIVE':
                        element['multiplier'] = 5
                    elif word == 'DOUBLE':
                        element['multiplier'] = element['multiplier'] * 2
                elif word in UnicodeAnalyzer.symbols.keys():
                    element['symbol'] = UnicodeAnalyzer.symbols[word]
                elif word in UnicodeAnalyzer.post_modifiers and word == 'BELOW':
                    element['add_after'] = True

            if 'add_after' in element or element['symbol'] == UnicodeAnalyzer.symbols['CAPITAL']:
                symbol += element['symbol'] * element['multiplier']
            else:
                symbol = element['symbol'] * element['multiplier'] + symbol

        if not symbol:
            return self.symbol

        return symbol


    def print_debug(self):
        ''' print Unicode debug information '''

        from pprint import pprint
        pprint(vars(self))

        print('original was \'{:s}\'. returned \'{:s}\''.format(self.symbol, self.extract_code()))
