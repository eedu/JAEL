''' module responsible for CodeCompleter class '''

from utils.config import Config

class CodeCompleter:
    ''' class CodeCompleter '''

    struct_symbols = ['#', '?', '¿', '_', '[']
    end_symbols = [',']

    @staticmethod
    def is_complete(code):
        ''' :return: True if all structures have a END token '''

        struct_count = sum([code.count(x) for x in CodeCompleter.struct_symbols])
        end_count = sum([code.count(x) for x in CodeCompleter.end_symbols])

        return struct_count == end_count

    @staticmethod
    def complete(code):
        ''' :return: the code with missing END tokens at the end '''

        if Config.debug['code_complete']:
            print('ORIGINAL CODE  (length={:d}):\n{:s}'.format(len(code), code))

        struct_count = sum([code.count(x) for x in CodeCompleter.struct_symbols])
        end_count = sum([code.count(x) for x in CodeCompleter.end_symbols])

        if Config.debug['code_complete']:
            print('COMPLETED CODE (length={:d}):\n{:s}'.format(len(code), code))

        return code + ',' * (struct_count - end_count)
