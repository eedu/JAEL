''' modules responsible for executing commands '''

import abc
import math

from functools import reduce

from execution.number_set import NumberSet
from execution.memory import GlobalValues
from utils.config import Config
from utils.exceptions import Break

class Command:
    '''abstract Command class'''
    __metaclass__ = abc.ABCMeta

    @staticmethod
    @abc.abstractmethod
    def run(command, machine):
        '''
        abstract method for running a command

        :param machine: machine used in execution
        '''

    @staticmethod
    def explain(command):
        ''' explains a program using pseudo-code '''

        from utils import code_explainer

        return code_explainer.explain_program(command)

class Func(Command):
    ''' class responsible for all functions '''

    functions = {
        #ARITMETIC
        'a': lambda s, h, t: (lambda y, x: x + y)(s.pop(), s.pop()),
        'b': lambda s, h, t: int(s.pop()),
        'c': lambda s, h, t: (lambda y, x: x ** (1/float(y)))(s.pop(), s.pop()),
        'd': lambda s, h, t: (lambda x: x - int(x))(s.pop()),
        'e': lambda s, h, t: (lambda y, x: x - y)(s.pop(), s.pop()),
        'g': lambda s, h, t: (lambda y, x: x ** y)(s.pop(), s.pop()),
        'h': lambda s, h, t: (lambda y, x: math.log(x, y))(s.pop(), s.pop()),
        'i': lambda s, h, t: (lambda y, x: x % y)(s.pop(), s.pop()),
        'k': lambda s, h, t: (lambda f, v:f(f, v))(lambda f, x, t=2: t  if x == 0 else (f(f, x-1, t+1) if all((t+1)%i for i in range(2,t)) else f(f, x, t+1)), int(s.pop())),
        'l': lambda s, h, t: math.factorial(s.pop()),
        'n': lambda s, h, t: NumberSet(list(range(int(s.pop())))),
        'o': lambda s, h, t: (lambda y, x: x * y)(s.pop(), s.pop()),
        'p': lambda s, h, t: reduce(lambda y, x: x + y, s.pop()),
        't': lambda s, h, t: GlobalValues.save(s.pop()),
        'u': lambda s, h, t: (lambda y, x: x / y)(s.pop(), s.pop()),
        'x': lambda s, h, t: reduce(lambda y, x: x * y, s.pop()),
        'y': lambda s, h, t: 1 if s.pop() == s.pop() else 0,
        '@': lambda s, h, t: (lambda n: reduce(lambda x, n: [x[1], x[0]+x[1]], range(n), [1, 1])[0])(int(s.pop())),
        #FLOW MANIPULATION
        'w': lambda s, h, t: Break().lambda_raise() if int(s.pop()) == 1 else None,
        #STACK MANIPULATION
        'π': lambda s, h, t: math.pi,
        'ℇ': lambda s, h, t: math.e,
        '&': lambda s, h, t: GlobalValues.read_iteration(),
        '⑀': lambda s, h, t: GlobalValues.load(),
        '˚': lambda s, h, t: 0,
        '`': lambda s, h, t: 1,
        '^': lambda s, h, t: 2,
        '~': lambda s, h, t: 3,
        'ˇ': lambda s, h, t: 10,
        '´': lambda s, h, t: [x for x in [s.pop()] for _ in range(2)],
        '.': lambda s, h, t: h.read(),
        '¯': lambda s, h, t: [s.pop(), s.pop()],
        '!': lambda s, h, t: h.write(s.pop()),
        '+': lambda s, h, t: (lambda y, x: NumberSet(x, y))(s.pop(), s.pop()),
        '=': lambda s, h, t: NumberSet(h.values),
        #TAPE HEAD MANIPULATION
        '>': lambda s, h, t: h.move(1),
        '<': lambda s, h, t: h.move(-1),
        '⋀': lambda s, h, t: t.activate_tape("-1"),
        '⋁': lambda s, h, t: t.activate_tape("+1")
    }

    @staticmethod
    def run(command, machine):
        function = command['exec']

        if function not in Func.functions:
            return

        if Config.debug['execution']:
            print('Executing {:s}'.format(function))
            import time
            time.sleep(1)

        value = None

        from contextlib import suppress
        with suppress(ValueError, ZeroDivisionError, OverflowError):
            value = Func.functions[function](machine.stack,
                                             machine.get_current_tape(),
                                             machine.tapes)

        if value is None:
            #print('An exception ocurred when executing \'{:s}\''.format(f))
            #print('Setting result to 0')
            pass
        elif isinstance(value, list):
            for val in value:
                machine.stack.push(val)
        elif isinstance(value, str):
            for val in value:
                run({'type': 'func', 'exec': val}, machine)
        else:
            machine.stack.push(value)

        if Config.debug['execution']:
            Print.run('', machine)

class If(Command):
    ''' class responsible for if statements '''

    @staticmethod
    def run(command, machine):
        if int(machine.stack.pop()) != 0:
            machine.execute(command['exec_true'])
        else:
            machine.execute(command['exec_false'])

class Loop(Command):
    ''' class responsible for loop structures '''

    @staticmethod
    def run(command, machine):
        GlobalValues.enter_iteration()
        while True:
            try:
                machine.execute(command['exec'])
            except Break:
                break
            GlobalValues.update_iteration()

        GlobalValues.leave_iteration()


class Repeat(Command):
    ''' class responsible for 'repeat' structures '''

    @staticmethod
    def run(command, machine):
        GlobalValues.enter_iteration()
        for i in range(int(machine.stack.pop())):
            try:
                machine.execute(command['exec'])
            except Break:
                break
            GlobalValues.update_iteration()

        GlobalValues.leave_iteration()

class Foreach(Command):
    ''' class responsible for 'foreach' structures '''

    @staticmethod
    def run(command, machine):
        values = machine.stack.pop()

        GlobalValues.enter_iteration()
        for val in values:
            machine.stack.push(val)
            try:
                machine.execute(command['exec'])
            except Break:
                break
            GlobalValues.update_iteration()

        GlobalValues.leave_iteration()

class Stack(Command):
    ''' class responsible for stacking values '''

    @staticmethod
    def run(command, machine):
        machine.stack.push(command['value'])

class Print(Command):
    ''' class responsible for the print function '''

    @staticmethod
    def run(command, machine):

        if Config.debug['global_values']:
            GlobalValues.print()

        if Config.debug['stack']:
            print(machine.stack)

        if Config.output_mode == 1:
            print('{:g}'.format(machine.get_current_tape().read()))
        elif Config.output_mode == 2:
            print(machine.get_current_tape())
        elif Config.output_mode == 3:
            print(machine.tapes)

COMMANDS = {
    'func': Func,
    'if': If,
    'loop': Loop,
    'repeat': Repeat,
    'foreach': Foreach,
    'stack': Stack,
    'print': Print
}


def run(command, machine):
    ''' checks the command type and give it to the appropriate class '''

    if command is None:
        return

    if isinstance(command, list):
        for com in command:
            run(com, machine)
    else:
        COMMANDS[command['type']].run(command, machine)
