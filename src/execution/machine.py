''' module that holds the machine class '''

from execution.memory import Stack, Setapes, GlobalValues
from execution import command as Command

from utils.exceptions import Break, Shutdown

class Machine:
    ''' class that holds all important objects used in execution '''

    def __init__(self):
        self.running = True
        self.stack = Stack()
        self.tapes = Setapes()

    @staticmethod
    def complete(program):
        ''' completes a specified program with necessary functions '''

        print_command = {'type': 'print'}
        if isinstance(program, list) and program[-1]['type'] != 'print':
            program.append(print_command)
        elif isinstance(program, dict) and program['type'] != 'print':
            program = [program, print_command]

        return program

    def execute(self, program):
        ''' executes a program on the machine '''

        try:
            Command.run(program, self)
        except Break as inst:
            if GlobalValues.read_iteration() is not None:
                raise inst
            else:
                self.shutdown()

    def get_current_tape(self):
        ''' :return: the current active tape '''

        return self.tapes.get_current_tape()

    def shutdown(self):
        ''' throws Shutdown exception '''
        self.running = False

        raise Shutdown
