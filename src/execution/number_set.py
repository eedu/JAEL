''' module responsible for NumberSet class '''
#import operator.py

class NumberSet:
    ''' class responsible for the NumberSet object
        it is capable of saving a set of numbers,
        and apply operations over all of them '''

    def __init__(self, e1, e2=None):
        if isinstance(e1, NumberSet):
            self.values = e1.values.copy()
        elif isinstance(e1, list):
            self.values = e1.copy()
        else:
            self.values = [e1]

        if e2 is not None:
            self.values.extend(e2.values if isinstance(e2, NumberSet) else [e2])

    def __eq__(self, other):

        if isinstance(other, NumberSet):
            return list(self) == list(other)
        if other is not None:
            return False

        return float(self) == float(other)

    def __add__(self, other):

        if isinstance(other, NumberSet):
            return NumberSet([x + sum(other.values) for x in self.values])

        return NumberSet([x+other for x in self.values])

    def __sub__(self, other):

        if isinstance(other, NumberSet):
            return NumberSet([x - sum(other.values) for x in self.values])

        return NumberSet([x-other for x in self.values])

    def __mul__(self, other):
        from functools import reduce

        if isinstance(other, NumberSet):
            return NumberSet([x * reduce(lambda p, q: p * q, other.values) for x in self.values])

        return NumberSet([x*other for x in self.values])

    def __truediv__(self, other):

        if isinstance(other, NumberSet):
            return NumberSet([x / sum(other.values) for x in self.values])

        return NumberSet([x / other for x in self.values])

    def __mod__(self, other):
        if isinstance(other, NumberSet):
            return NumberSet([x % sum(other.values) for x in self.values])

        return NumberSet([x % other for x in self.values])

    def __pow__(self, other):

        if isinstance(other, NumberSet):
            return NumberSet([x ** sum(other.values) for x in self.values])

        return NumberSet([x ** other for x in self.values])

    def __iter__(self):
        return iter(self.values)

    def __int__(self):
        return int(sum(self.values))

    def __float__(self):
        return float(sum(self.values))

    def __list__(self):
        return self.values.copy()

    def __len__(self):
        return len(self.values)

    def __str__(self):
        return '{:s}'.format('[{:g}]'.format(self.values[0]) if len(self.values) == 1
                             else '({:s})'.format(', '.join(['{:g}'.format(x)
                                                             for x in self.values])))
