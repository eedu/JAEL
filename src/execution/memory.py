''' module responsible for memory-like objects '''

from utils.config import Config
from execution.number_set import NumberSet

class Stack:
    ''' class Stack '''

    def __init__(self):
        self.values = []

    def push(self, value):
        ''' pushes the value to the stack
        and updates the GlobalValues accordingly '''

        if value is not None:
            if Config.debug['execution']:
                print('pushing {:s}...'.format(str(value)))
            self.values.append(NumberSet(value))
            GlobalValues.update_last_pushed(value)

    def pop(self):
        ''' pops the top value fom the stack
        and updates the GlobalValues accordinly '''

        if not self.values:
            value = NumberSet(0)
        else:
            value = self.values.pop()

        GlobalValues.update_last_popped(value)

        if Config.debug['execution']:
            print('popping {:s}...'.format(str(value)))
        return value

    def __len__(self):

        return len(self.values)

    def __str__(self):
        main_str = 'Stack: \n\tLength: {:d}{:s}\n'
        tape_str = ''.join('\n\t{:d}: {:s}'
                           .format(len(self) - i - 1,
                                   '{:g}'.format(x) if isinstance(x, float) else str(x))
                           for i, x in enumerate(self.values)
                           )
        return main_str.format(len(self), tape_str)

class Tape:
    ''' class Tape '''

    def __init__(self):
        self.values = [0]
        self.head_pos = 0

    def read(self):
        ''' :return: the value under the tape head '''
        value = self.values[self.head_pos]
        GlobalValues.update_last_read(value)
        return value

    def set(self, value):
        ''' changes the tape values to
        the list received as parameter '''

        self.values = list(value)
        self.head_pos = 0

        GlobalValues.update_last_written(value)

    def write(self, value):
        ''' :param value: the value to be written
        on the tape head position '''

        if isinstance(value, (list, NumberSet)) and len(value) > 1:
            self.set(value)
        else:
            self.values[self.head_pos] = int(value) if int(value) == float(value) else float(value)

        GlobalValues.update_last_written(value)

    def move(self, amount):
        ''' :param amount: the amount of nodes to
        move the tape head (negative number to go
        to the left '''

        self.head_pos += amount
        while self.head_pos < 0:
            self.values = [0] + self.values
            self.head_pos += 1
        while self.head_pos >= len(self.values):
            self.values = self.values + [0]

    def __str__(self):
        if Config.debug['tape']:
            head_symbol = '↑'
            head_up = False
            f_f = '{:s}' if len(head_symbol) % 2 == 0 else ' {:s}'
            f_s = str

            float_head_value = len(f_f.format(
                f_s(self.values[self.head_pos])))
            elements = '[' + ','.join(f_f.format(f_s(x)) for x in self.values) + ']'

            head_mark = ' {:s}{:s}{:s}'.format(' ' * (1 + sum([len(f_f.format(f_s(x))) + 1
                                                               for x in self.values[:self.head_pos]])),
                                               ' ' * (int(float_head_value/2 - len(head_symbol)) % 2),
                                               '{:s}'.format(head_symbol if not head_up
                                                             else head_symbol[::-1]))
            return '\n'.join([elements, head_mark] if not head_up else [head_mark, elements])

        return str(self.values).replace('[', '').replace(']', '')

class Setapes:
    ''' class Setapes

        responsible for the sets of tapes
        and their actions
    '''

    def __init__(self):
        self.tapes = [Tape()]
        self.activate_tape()

    def get_current_tape(self):
        ''' :return: the current tape '''

        return self.tapes[self.current_tape]

    def activate_tape(self, index=0):
        ''' activates a tape according to the index parameter
            :param index: if is str, it is considered as a
            calculation over the current value (like '+2')
            and applied as such
        '''

        if isinstance(index, str):
            self.current_tape = eval('{:d} {:s}'.format(self.current_tape, index))
            while self.current_tape < 0:
                self.tapes.insert(0, Tape())
                self.current_tape += 1
            while self.current_tape >= len(self.tapes):
                self.tapes.append(Tape())

        elif isinstance(index, int) and index >= 0 and index < len(self.tapes):
            self.current_tape = index
        else:
            self.current_tape = len(self.tapes) - 1

    def __str__(self):
        if Config.debug['tape']:
            main_str = 'Tapes: \n\tLength: {:d}{:s}\n'

            tapes_str = ''.join('\n\t{:s}{:s}{:d}: {:s}'
                                .format('→' if i == self.current_tape else ' ',
                                        ' ' * (1 + len(str(len(self.tapes)-1)) - len(str(i))),
                                        i,
                                        str(x).replace('\n', '\n\t    '
                                                       + ' ' * len(str(len(self.tapes)-1)))
                                        ) for i, x in enumerate(self.tapes))
            return main_str.format(len(self.tapes), tapes_str)

        main_str = '{:s}'

        tapes_str = '\n'.join('{:s}'.format(str(x)) for i, x in enumerate(self.tapes))
        return main_str.format(tapes_str)



class GlobalValues:
    ''' class GlobalValues

        responsible for saving and retrieving
        important values from execution
    '''

    v_iterations_count = []
    v_last_pushed = None
    v_last_popped = None
    v_last_read = None
    v_last_written = None
    v_saved = None

    @staticmethod
    def enter_iteration():
        ''' used to inform that a loop has been started '''

        GlobalValues.v_iterations_count.append(0)

    @staticmethod
    def leave_iteration():
        ''' used to inform that a loop has been finished '''
        if not GlobalValues.v_iterations_count:
            return

        del GlobalValues.v_iterations_count[-1]

    @staticmethod
    def update_iteration():
        ''' used to inform that a loop has been iterated '''

        GlobalValues.v_iterations_count[-1] += 1

    @staticmethod
    def read_iteration():
        ''' returns the number of times the current loop has been iterated '''
        if not GlobalValues.v_iterations_count:
            return None

        return GlobalValues.v_iterations_count[-1]

    @staticmethod
    def update_last_pushed(val):
        ''' used to inform the last pushed value to stack '''

        GlobalValues.v_last_pushed = val

    @staticmethod
    def update_last_popped(val):
        ''' used to inform the last pushed value from stack '''

        GlobalValues.v_last_popped = val

    @staticmethod
    def update_last_read(val):
        ''' used to inform the last read value from a tape '''

        GlobalValues.v_last_read = val

    @staticmethod
    def update_last_written(val):
        ''' used to inform the last written value onto a tape '''

        GlobalValues.v_last_written = val

    @staticmethod
    def save(val):
        ''' used to save a value into a variable '''

        GlobalValues.v_saved = val

    @staticmethod
    def load():
        ''' used to retrieve the saved value '''

        return GlobalValues.v_saved

    @staticmethod
    def print():
        ''' prints the defined values '''

        values = [(i, j) for i, j in GlobalValues.__dict__.items()
                  if i.startswith('v_')]
        print('\n'.join(['{:s}: {:s}'.format(x[0], str(x[1]))
                         for x in values]))
