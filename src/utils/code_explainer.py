''' module responsible for explaining a program, in pseudocode '''

FUNC_EXPLANATIONS = {
    'a': 'push p1 + p2',
    'b': 'push int(p1)',
    'c': 'push root p1 of p2',
    'd': 'push p1 - int(p1)',
    'e': 'push p2 - p1',
    'g': 'push p2 ^ p1',
    'h': 'push log base p1 of p2',
    'i': 'push p2 % p1',
    'k': 'push nth prime',
    'l': 'push p1!',
    'n': 'push [0...p1]',
    'o': 'push p1 * p2',
    'p': 'push the sum of all values of set(p1)',
    't': 'save p1',
    'u': 'push p2 / p1',
    'x': 'push the product of all values of set(p1)',
    'y': 'push 1 if p1 == p2',
    'w': 'break if p1 == 1',
    'π': 'push pi',
    'ℇ': 'push e',
    '⑀': 'load saved value',
    '&': 'push number of iterations of this loop',
    '@': 'push nth fibonacci number',
    '˚': 'push 0',
    '`': 'push 1',
    '^': 'push 2',
    '~': 'push 3',
    'ˇ': 'push 10',
    '¯': 'invert p1 and p2 on the stack',
    '´': 'duplicate p1',
    '.': 'push the value under the tape head',
    '-': 'push p1 and p2 switched',
    '!': 'write p1 to the tapehead',
    '>': 'move the tapehead to the right',
    '<': 'move the tapehead to the left',
    '⋀': 'go to the tape above',
    '⋁': 'go to the tape below',
}

CODE_PRINT_PATTERNS = {
    'print': '␄',
    'stack': '{0[symbol]}',
    'func': '{0[symbol]}',
    'if': '?{0[spcs_0]},\n{0[code_0]}',
    'ifelse': '?{0[spcs_0]}|{0[spcs_1]},\n{0[code_0]}\n{0[spcs_0]} |{0[spcs_1]},\n{0[code_1]}',
    'repeat': '#{0[spcs_0]},\n{0[code_0]}',
    'foreach': '_{0[spcs_0]},\n{0[code_0]}',
    'loop': '[{0[spcs_0]},\n{0[code_0]}',
    'error': ' '
}

EXP_PRINT_PATTERNS = {
    'print': 'print machine state',
    'stack': 'stack {0[value]}',
    'func': '{0[exp_0]}',
    'errorfunc': 'ERROR: key {0[symbol]} not found',
    'if': 'if (p1) != 0:\n{0[exp_0]}',
    'ifelse': 'if (p1) != 0:\n{0[exp_0]}\nelse:\n{0[exp_1]}',
    'repeat': 'repeat (p1) times:\n{0[exp_0]}',
    'foreach': 'for each value (v) in the tape, push(v) and run:\n{0[exp_0]}',
    'loop': 'loop:\n{0[exp_0]}',
    'error': 'UNKNOWN ERROR'
}

def explain(code):
    ''' explains the code in pseudocode '''

    from execution.machine import Machine
    from processing import parser as Parser
    from processing.code_expander import CodeExpander
    from processing.code_completer import CodeCompleter

    code = CodeExpander.expand(code)
    code = CodeCompleter.complete(code)
    program = Parser.parse(code)
    program = Machine().complete(program)

    return explain_program(program)

def explain_program(program):
    ''' explains completed progam '''

    explained_code = __explain_rec(program)
    code_side = explained_code[0]
    exp_side = explained_code[1]

    return join_explanations(code_side, exp_side)

def join_explanations(code_side, exp_side):
    ''' :return: both parts of explanation side to side '''

    code_side = code_side.split('\n')
    exp_side = exp_side.split('\n')

    max_code_len = max([len(s) for s in code_side])
    explanation = '\n'.join(['{:s}{:s}\t\t{:s}'.format(code_side[i],
                                                       ' ' * (max_code_len - len(code_side[i])),
                                                       exp_side[i])
                             for i in range(len(exp_side))])

    return explanation

def __explain_rec(program):
    if program is None:
        return ('', '')
    if isinstance(program, list):
        rec_call = [__explain_rec(f) for f in program]
        code_arr = [f[0] for f in rec_call]
        code_arr_lens = [max([len(y) for y in x.split('\n')]) for x in code_arr]
        code_arr_spcs = [' ' * sum(code_arr_lens[:i]) for i in range(len(code_arr_lens))]

        code_str = '\n'.join(['\n'.join(['{:s}{:s}'
                                         .format(code_arr_spcs[i], x)
                                         for x in code_arr[i].split('\n')
                                        ]) for i in range(len(rec_call))
                             ])
        exp_str = '\n'.join([f[1] for f in rec_call])

        return (code_str, exp_str)

    return __explain_join(program)

def __explain_join(program):

    pattern_check = program['type']

    print_values = {}

    if program['type'] == 'stack':

        print_values['symbol'] = program['symbol']
        print_values['value'] = program['value']

    elif program['type'] == 'func':
        print_values['symbol'] = program['exec']

        if program['exec'] in FUNC_EXPLANATIONS:
            print_values['exp_0'] = FUNC_EXPLANATIONS[program['exec']]
        else:
            pattern_check = 'error'

    elif program['type'] != 'print':

        if program['type'] == 'if':
            execs = [program['exec_true']]
            if 'exec_false' in program and program['exec_false'] is not None:
                execs.append(program['exec_false'])
                pattern_check = 'ifelse'
        else:
            execs = [program['exec']]

        ac_spcs = ''
        for i, exe in enumerate(execs):
            code_arr, exp_arr = __explain_rec(exe)
            max_code_len = max([len(f) for f in code_arr.split('\n')])

            print_values['spcs_{:d}'.format(i)] = ' ' * max_code_len

            print_values['code_{:d}'.format(i)] = '\n'.join(['{:s} {:s}'.format(ac_spcs, s)
                                                             for s in code_arr.split('\n')])
            print_values['exp_{:d}'.format(i)] = '\n'.join(['\t{:s}'.format(s)
                                                            for s in exp_arr.split('\n')])

            ac_spcs += ' ' * max_code_len + ' '

    code_str = CODE_PRINT_PATTERNS[pattern_check].format(print_values)
    exp_str = EXP_PRINT_PATTERNS[pattern_check].format(print_values)

    return (code_str, exp_str)
