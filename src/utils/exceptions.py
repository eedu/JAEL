''' module that holds all exceptions of JAEL execution '''

class JAELxception(Exception):
    ''' Main jael exception '''

    def lambda_raise(self):
        ''' It makes possible to raise an exception from a lambda function '''
        raise type(self)

class Break(JAELxception):
    ''' exception sent when code needs to break from a loop '''

class Shutdown(JAELxception):
    ''' exception sent when the machine needs to stop execution '''

class FinishExecution(JAELxception):
    ''' exception sent when the interpreter needs to quit the application '''
