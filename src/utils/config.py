''' module config '''

class Config:
    ''' class config '''

    debug_level = 0
    output_mode = 3

    compare_mode = 'bytes'
   #compare_mode = 'chars'

    debug_options = ['stack',               #   1
                     'input',               #   2
                     'global_values',       #   4
                     'tape',                #   8
                     'code_length',         #  16
                     'code_expand',         #  32
                     'code_complete',       #  64
                     'program_structure',   # 128
                     'unicode',             # 256
                     'execution',           # 512
                     'code_compress']       #1024
    debug = {}

    @staticmethod
    def set_debug(debug_level):
        ''' changes the config debug_level '''

        Config.debug_level = debug_level

        for i in range(len(Config.debug_options)):
            opt = len(Config.debug_options) - 1 - i
            val = 2 ** opt
            if debug_level >= val:
                debug_level -= val
                Config.debug[Config.debug_options[opt]] = True
            else:
                Config.debug[Config.debug_options[opt]] = False

        if Config.debug_level > 0:
            print(Config.debug)



    @staticmethod
    def get_debug():
        ''' :return: the current debug level '''

        return Config.debug_level
