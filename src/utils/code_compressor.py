''' module responsible for compressing code while keeping it equivalent '''

from processing.code_expander import UnicodeAnalyzer

from utils.config import Config

# Here we just need to add symbols that can be processed by the UnicodeAnalyzer.
# Other characters are automatically added later
COMPRESSED_COMMANDS = [chr(c) for c in range(65535)]

POSSIBLE_COMPRESSIONS = {}
LOADED = False

def compare(code):
    ''' returns the size of the code according to the selected compare function '''

    return {'chars': len, 'bytes': lambda s: len(s.encode('utf-8'))}[Config.compare_mode](code)

def is_loaded():
    ''' checks if the expanded versions of compressed commands are loaded '''
    return LOADED

def load_compressions():
    '''
        executes code expansion over each compressed command
        to retrieve it's expanded version
    '''

    if is_loaded():
        return

    for command in COMPRESSED_COMMANDS:
        u_a = UnicodeAnalyzer(command)
        ext_code = u_a.extract_code()
        if ext_code not in POSSIBLE_COMPRESSIONS:
            #print('{:s} -> {:s} ({:d})'.format(command, ext_code, ord(command)))
            POSSIBLE_COMPRESSIONS[ext_code] = command

    from processing.code_expander import MappedExtractor

    for ext in MappedExtractor.extractions:
        POSSIBLE_COMPRESSIONS[MappedExtractor.extractions[ext]] = ext


    #print(len(POSSIBLE_COMPRESSIONS))

VERSIONS = []

def __compress_rec(code):
    ''' recursive version of the compress method '''

    global VERSIONS

    if code in VERSIONS:
        return

    if Config.debug['execution']:
        min_ver = compare(code)
        for version in VERSIONS:
            if compare(version) <= min_ver:
                min_ver = compare(version)

        if min_ver == compare(code):
            print('{:s} (size: {:d})'.format(code, compare(code)))


    VERSIONS.append(code)
    for chars in POSSIBLE_COMPRESSIONS:
        i = -1
        while True:
            try:
                i = code.index(chars, i+1)
                new_code = code[0:i] + POSSIBLE_COMPRESSIONS[chars] + code[i+len(chars):len(code)]
                __compress_rec(new_code)
            except ValueError:
                break

def is_equivalent(original_code, compressed_code):
    ''' verifies if the compressed version of code
        is equivalent to the original one '''

    from processing.code_expander import CodeExpander
    from processing.code_completer import CodeCompleter

    original_code = CodeExpander.expand(original_code)
    original_code = CodeCompleter.complete(original_code)

    compressed_code = CodeExpander.expand(compressed_code)
    compressed_code = CodeCompleter.complete(compressed_code)

    return original_code == compressed_code


def compress(code):
    ''' :return: the smallest code that is compatible with :param code: '''
    load_compressions()

    if Config.debug['code_compress']:
        print('ORIGINAL CODE\t(size: {:d} {:s}): {:s}'.format(compare(code), Config.compare_mode, code))

    global VERSIONS

    VERSIONS = []

    from processing.code_expander import CodeExpander
    code = CodeExpander.expand(code)
    code = code.strip(',')


    __compress_rec(code)

    for version in VERSIONS:
        if ((compare(version) < compare(code)
             or (compare(version) == compare(code) and len(version) < len(code)))
                and is_equivalent(version, code)):
            code = version

    code = code.strip(',')

    if Config.debug['code_compress']:
        print('COMPRESSED CODE\t(size: {:d} {:s}): {:s}'.format(compare(code), Config.compare_mode, code))

    return code
