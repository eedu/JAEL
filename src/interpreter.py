''' module responsible for real time code execution'''

from execution.machine import Machine
from processing import parser as Parser
from processing.code_expander import CodeExpander
from processing.code_completer import CodeCompleter
from utils.exceptions import Shutdown, FinishExecution
from utils.config import Config


def interactive():
    ''' interactive execution '''

    machine = Machine()

    while True:
        try:
            code = input('jael: ') + '\n'
            execute(code, None, machine)
        except Shutdown:
            print("Exception")
            raise FinishExecution

def execute(code, code_input=None, machine=Machine()):
    '''
        :param code: raw code to be executed
    '''

    if Config.debug['code_length']:
        print('Code length: {:d}'.format(len(code.replace('\n', ''))))

    if code_input is not None:
        for i in code_input:
            machine.stack.push(i)

    code = CodeExpander.expand(code)
    code = CodeCompleter.complete(code)

    program = Parser.parse(code)
    program = Machine.complete(program)

    try:
        machine.execute(program)
    except Shutdown:
        print('Execution aborted')
        raise FinishExecution
