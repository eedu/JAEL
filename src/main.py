'''Main JAEL module'''

import interpreter
from processing.code_completer import CodeCompleter
from processing.code_expander import CodeExpander
from utils import code_compressor, code_explainer
from utils.config import Config
from utils.exceptions import FinishExecution

def prepare_argument_parser():
    ''' Use arparse library to parse arguments
        :return: The args object
    '''
    import argparse
    import textwrap
    import os
    import sys

    if 'SCRIPTCALL' in os.environ:
        scriptcall = os.environ['SCRIPTCALL']
    else:
        scriptcall = 'python {:s}'.format(sys.argv[0])

    parser = argparse.ArgumentParser(scriptcall, formatter_class=argparse.RawTextHelpFormatter)
    group1 = parser.add_mutually_exclusive_group(required=False)
    group1.add_argument('-i', action='store_true',
                        help='run in interactive mode')
    group1.add_argument('--complete', type=str, metavar='CODE', help='print completed code')
    group1.add_argument('--compress-bytes', type=str, metavar='CODE', help='print code compressed for byte reduction')
    group1.add_argument('--compress-chars', type=str, metavar='CODE', help='print code compressed for utf-8 char reduction')
    group1.add_argument('--expand', type=str, metavar='CODE', help='print expanded code')
    group1.add_argument('--explain', type=str, metavar='CODE', help='print explained code')

    parser.add_argument('-d', metavar='DEBUG_LEVEL', default=0, type=int,
                        help=textwrap.dedent('''
                                             set debug level. Sum the desired options
                                                1: stack
                                                2: input
                                                4: global values
                                                8: tape
                                               16: length of code
                                               32: code expansion
                                               64: code completion
                                              128: program structure
                                              256: unicode
                                              512: execution
                                             ''').strip())
    parser.add_argument('code', metavar='CODE', type=str, nargs='?',
                        help='code to be executed')

    return parser

def check_environment():
    '''Check if is all set to run the interpreter'''

    import sys

    current_encoding = str(sys.stdout.encoding).upper()

    if current_encoding != 'UTF-8':
        print('''Current stdout encoding is {:s}.
                You should set the Python I/O encoding to UTF-8
                
                Try one of the following:
                \texport PYTHONIOENCODING=UTF-8
                \texport PYTHONUTF8=1
                \tpython -X utf8 {:s}'''.format(current_encoding, ' '.join(sys.argv)))
        exit(1)

def gather_input():
    ''' gets user input until an empty line is sent '''

    lines = []
    inpt = None
    while True:
        try:
            inpt = input()
        except EOFError:
            break

        if inpt == '':
            break

        lines.append([(lambda x: int(x) if int(x) == float(x) else float(x))(eval(x)) for x in inpt.split(' ')])

    return lines

def main():
    '''The main function'''

    check_environment()

    parser = prepare_argument_parser()
    args = parser.parse_args()

    Config.set_debug(args.d)

#    if 'output_mode' in args and args.output_mode is not None:
#        Config.output_mode = args.output_mode

    if args.i:
        interactive()
    elif args.code is not None:
        inpt = gather_input()
        execute(args.code, inpt)
    elif args.complete is not None:
        code = args.complete
        Config.debug['code_complete'] = True

        code = CodeCompleter.complete(code)

    elif args.compress_bytes is not None:
        mode = 'bytes'
        code = args.compress_bytes

        Config.debug['code_compress'] = True
        Config.compare_mode = mode

        code = code_compressor.compress(code)

    elif args.compress_chars is not None:
        mode = 'chars'
        code = args.compress_chars

        Config.debug['code_compress'] = True
        Config.compare_mode = mode

        code = code_compressor.compress(code)
    elif args.expand is not None:
        code = args.expand

        Config.debug['code_expand'] = True
        Config.debug['code_complete'] = True

        code = CodeExpander.expand(code, complete=True)

    elif args.explain is not None:
        code = args.explain

        print('ORIGINAL CODE:\t{:s}\n'.format(code))

        expandings = [(x, CodeExpander.expand(x)) for x in code]
        expandings = [x for x in expandings if x[0] != x[1]]

        if expandings:
            print('EXPANDING EXPLANATION: \n{:s}\n'
                  .format('\n'.join(['{:s} => {:s}'.format(x[0], x[1]) for x in expandings])))

        expanded_code = CodeExpander.expand(code)
        if expanded_code != code:
            print('EXPANDED CODE:\t{:s}\n'.format(expanded_code))

        completed_code = CodeCompleter.complete(expanded_code)

        if completed_code != expanded_code:
            print('COMPLETED CODE:\t{:s}\n'.format(completed_code))

        explained_code = code_explainer.explain(completed_code)
        print(explained_code)

    else:
        parser.print_help()


def interactive():
    ''' Method for interactive execution '''

    try:
        interpreter.interactive()
    except FinishExecution:
        pass

def execute(code, code_input=None):
    ''' Method for execute code onde
        :param code: raw code to be executed
        '''
    try:
        interpreter.execute(code, code_input)
    except FinishExecution:
        pass

if __name__ == '__main__':
    main()
