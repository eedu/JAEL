# Contributing

Any contributions are accepted. However, some of them are more necessary right now. They are:

* To test and look for bugs
* Suggestions about new functions
* More code examples
* Real usage in Code Golf challenges (that would be awesome)
