# JAEL - Just Another Esoteric Language

<!--[![Current release](https://img.shields.io/github/release/eduardohoefel/jael.svg)](https://github.com/eduardohoefel/jael/releases)-->

[![GitHub tag](https://img.shields.io/github/tag/eduardohoefel/jael.svg)](https://github.com/eduardoHoefel/JAEL/releases)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/eduardohoefel/jael.svg)
[![GitHub issues](https://img.shields.io/github/issues/eduardohoefel/jael.svg?maxAge=360)](https://github.com/eduardohoefel/jael/issues)
[![LICENSE](https://img.shields.io/github/license/eduardoHoefel/JAEL.svg)](https://github.com/eduardohoefel/jael/LICENSE)



## About

JAEL is a programming language developed to create solutions to mathematical challenges, like [Project Euler's](http://projecteuler.net) or [Code Golf](https://en.wikipedia.org/wiki/Code_golf).
Therefore, it is considered a _Golfing Language_.

Currently, it only computes numbers, sets, and matrices, so it is not capable of handling Strings at all, nor any other data structure. It was built to be more like a 'complex calculator' than a programming language.

This project is being developed as part of of my Computer Science undergraduate thesis, which is going to be presented in December 2018 :)

## Documentation

Please read the [wiki](https://github.com/eduardoHoefel/JAEL/wiki).

## System Requirements

* Python 3.6
* Ply (pip install ply)

## Usage

After having Python 3.6 and Ply installed, you can just run

```sh
$ ./jael -i
```

from a Bash terminal.

If you are in a Windows environment or simply don't have Bash for some reason, the command

```sh
$ python -X utf8 src/main.py -i
```

should work as well. However, some unicode characters might not be correctly rendered as the Windows terminals do not support Unicode really well.

## Contributions

See the [CONTRIBUTING.md](CONTRIBUTING.md) file for how to contribute with JAEL development.

## TODO

JAEL is still in development. There are plenty of things to implement.

## License

See the [LICENSE](LICENSE) file for license rights and limitations (GPLv3).
